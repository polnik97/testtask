package ru.rti.task;

import ru.rti.task.configuration.InitialConfiguration;
import ru.rti.task.state.ServerState;

public interface Server {
    void configure(InitialConfiguration initialConfiguration);

    void run();

    void stop();

    ServerState getState();
}
