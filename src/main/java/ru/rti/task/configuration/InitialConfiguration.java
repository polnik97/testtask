package ru.rti.task.configuration;

import ru.rti.task.constants.ConfigurationFormat;

public interface InitialConfiguration {
    ConfigurationFormat getFormat();

    String getContent() throws Exception;
}