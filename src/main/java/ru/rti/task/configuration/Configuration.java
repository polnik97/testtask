package ru.rti.task.configuration;

import java.util.Map;

public interface Configuration {
    String getHostName();

    int getPort();

    Map<String, String> getErrorCodesMapping();

    int getHandledMessagesCount();
}
