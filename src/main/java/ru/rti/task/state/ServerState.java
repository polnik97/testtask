package ru.rti.task.state;

import ru.rti.task.configuration.Configuration;
import ru.rti.task.constants.ServerStatus;

public interface ServerState {
    Configuration getConfiguration();

    ServerStatus getStatus();
}