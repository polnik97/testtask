package ru.rti.task.constants;

public enum ConfigurationFormat {
    XML,
    JSON
}
