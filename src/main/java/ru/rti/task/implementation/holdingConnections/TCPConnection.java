package ru.rti.task.implementation.holdingConnections;

import java.io.*;
import java.net.Socket;
import java.nio.charset.Charset;

//Класс отвечающий за подключение пользователся, отправку и принятие сообщений от него

public class TCPConnection {
    private static Socket socket;
    //слушатель событий подключения
    private static TCPConnectionListener eventListener;
    //поток ввода
    private static BufferedReader in;
    //поток вывода
    private static BufferedWriter out;

    public TCPConnection(TCPConnectionListener eventListener, Socket socket) throws IOException
    {
        this.eventListener = eventListener;
        this.socket = socket;
        in = new BufferedReader(new InputStreamReader(socket.getInputStream(), Charset.forName("UTF-8")));
        out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream(), Charset.forName("UTF-8")));
        this.eventListener.onConnectionReady(this);
    }


    public String getMessage(){
        try {
            if(!socket.isClosed())
            {
                if(in.ready()) {
                    //считывание входящего сообщения
                   return in.readLine();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Socket getSocket() {
        return this.socket;
    }


    //отправка сообщения клиенту
    public synchronized void sendMessage(String msg)
    {
        try {
            out.write(msg + "\r\n");
            out.flush();
        }catch (IOException e){
            eventListener.onExeption(TCPConnection.this, e);
            disconnect();
        }
    }

    //отключение клиента
    public synchronized void disconnect()
    {
        try {
            socket.close();
        }catch (IOException e)
        {
            eventListener.onExeption(TCPConnection.this, e);
        }
    }
}
