package ru.rti.task.implementation.holdingConnections;

import ru.rti.task.implementation.messageSystem.Address;
import ru.rti.task.implementation.MessageDifinition.MessageDefinitor;
import ru.rti.task.implementation.messageSystem.MessageSystem;

import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

//Проверяет наличие сообщений от подключенных клиентов

public class ConnectionsChecker implements Runnable {

    CopyOnWriteArrayList connectios;
    private Address address = new Address();
    private final MessageSystem messageSystem;


    public ConnectionsChecker(ConnectionsHolder holder, MessageSystem messageSystem) {
        this.connectios = holder.getConnections();
        this.messageSystem = messageSystem;
        messageSystem.getAddressService().registerConnectionsChecker(this);
    }

    public Address getAddress() {
        return address;
    }

    public MessageSystem getMessageSystem() {
        return messageSystem;
    }

    @Override
    public void run() {
        String content;
        TCPConnection connection;
        ExecutorService pool = Executors.newFixedThreadPool(4);
        while(true) {
            if(!connectios.isEmpty()){
                Iterator<TCPConnection> iterator =  connectios.iterator();
                while(iterator.hasNext())
                {
                    connection = iterator.next();
                    content = connection.getMessage();
                    if(content != null)
                    {
                        pool.submit(new MessageDefinitor(this.messageSystem, connection, content));

                    }else{
                        continue;
                    }

                }
            }

        }
    }
}
