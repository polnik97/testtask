package ru.rti.task.implementation.holdingConnections;

import ru.rti.task.implementation.messageSystem.Abonent;
import ru.rti.task.implementation.messageSystem.Address;
import ru.rti.task.implementation.messageSystem.Message;
import ru.rti.task.implementation.messageSystem.MessageSystem;

import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

//Класс, хранящий подключенных пользователей и соответствуеющие им
// экземаляры класса TCPConnection

public class ConnectionsHolder {
    private final CopyOnWriteArrayList<TCPConnection> connections = new CopyOnWriteArrayList<TCPConnection>();


    public ConnectionsHolder()
    {
    }


    public CopyOnWriteArrayList<TCPConnection> getConnections() {
        return connections;
    }

    public void addConnection(TCPConnection connection)
    {
        connections.add(connection);
    }

    public void removeConnection(TCPConnection connection)
    {
        connections.remove(connection);
    }

}
