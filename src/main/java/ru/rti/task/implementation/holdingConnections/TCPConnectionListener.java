package ru.rti.task.implementation.holdingConnections;

public interface TCPConnectionListener {

    //дейстиве при подключение клиента
    void onConnectionReady(TCPConnection connection);
    //руеакция на получение сообщения
    void onReceieviMessage(TCPConnection connection, String msg);
    //реакция на ошибку
    void onExeption(TCPConnection connection, Exception e);
    //реакция на отключение
    void onDisconnect(TCPConnection connection);

}
