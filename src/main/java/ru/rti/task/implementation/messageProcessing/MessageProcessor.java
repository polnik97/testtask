package ru.rti.task.implementation.messageProcessing;

import ru.rti.task.implementation.messageSystem.Abonent;
import ru.rti.task.implementation.messageSystem.Address;
import ru.rti.task.implementation.messageSystem.Message;
import ru.rti.task.implementation.messageSystem.MessageSystem;

import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

//Проверяет наличие сообщений из MessageSystem, при наличиие запускает его на выполение в пулл
//Отвечает за обработку информации и формирование ответа

public final class MessageProcessor implements Abonent, Runnable{
    private final Address address = new Address();
    private final MessageSystem messageSystem;

    public MessageProcessor(MessageSystem messageSystem)
    {
        this.messageSystem = messageSystem;
        messageSystem.addService(this);
        messageSystem.getAddressService().registerMessageProcessor(this);
    }


    @Override
    public void run() {

        ExecutorService pool = Executors.newFixedThreadPool(3);
        while(true) {
            ConcurrentLinkedQueue<Message> messages = messageSystem.getMessages().get(address);
            while (!messages.isEmpty())
            {
                pool.submit(messages.poll());
            }
        }


    }

    @Override
    public Address getAddress() {
        return address;
    }
}
