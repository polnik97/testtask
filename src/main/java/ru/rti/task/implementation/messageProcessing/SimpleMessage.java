package ru.rti.task.implementation.messageProcessing;

import ru.rti.task.implementation.answeringService.SimpleAnswer;
import ru.rti.task.implementation.holdingConnections.TCPConnection;
import ru.rti.task.implementation.messageSystem.Address;
import ru.rti.task.implementation.messageSystem.Message;
import ru.rti.task.implementation.messageSystem.MessageSystem;

//Простой ипо ответа, формата "Hello, <имя пользователя>"

public class SimpleMessage extends Message implements Runnable {
    private TCPConnection connection;
    private String content;
    private MessageSystem messageSystem;
    public SimpleMessage(Address from, Address to, TCPConnection connection, String content, MessageSystem messageSystem) {
        super(from, to);
        this.connection = connection;
        this.content = content;
        this.messageSystem = messageSystem;
    }

    public TCPConnection getConnection() {
        return connection;
    }

    public void setConnection(TCPConnection connection) {
        this.connection = connection;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public void run() {
        String name;
        String text;
        int messageStartPos = content.indexOf(";message:");
        name = content.substring(5, messageStartPos);
        text = content.substring(messageStartPos, content.length() - 1);
        String answer = "Hello, " + name;
        Message msg = new SimpleAnswer(getTo(), messageSystem.getAddressService().getAnsweringService(), answer, connection );
        messageSystem.sendMessage(msg);
    }
}
