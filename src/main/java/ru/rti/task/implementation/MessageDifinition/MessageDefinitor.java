package ru.rti.task.implementation.MessageDifinition;

import ru.rti.task.implementation.answeringService.SimpleAnswer;
import ru.rti.task.implementation.messageProcessing.SimpleMessage;
import ru.rti.task.implementation.holdingConnections.TCPConnection;
import ru.rti.task.implementation.messageSystem.Message;
import ru.rti.task.implementation.messageSystem.MessageSystem;

//Определение типа запроса, пришедшего от пользователя, и формирование соответствующего
//сообщения к MessageProcessor для обработки и формирования ответа

public class MessageDefinitor implements Runnable {
    //private final ConcurrentHashMap<Class,  CopyOnWriteArrayList<String>> keyWords;
    private MessageSystem messageSystem;
    private TCPConnection connection;
    private String msg;

    public MessageDefinitor(MessageSystem messageSystem, TCPConnection connection, String msg)
    {
        this.connection = connection;
        this.messageSystem = messageSystem;
        this.msg = msg;
    }


    @Override
    public void run() {
        msg = msg.trim();
        //Определение типа сообщения
        if(msg.contains("message:") || msg.startsWith("user:"))
        {
            Message sendingMsg = new SimpleMessage(messageSystem.getAddressService().getAnsweringService(), messageSystem.getAddressService().getMessageProcessor(), connection, msg, messageSystem);
            messageSystem.sendMessage(sendingMsg);

        }else
        {
            //error message
            String error = messageSystem.getErrors().get("INVALID_REQUEST");
            Message sendingMsg = new SimpleAnswer(messageSystem.getAddressService().getAnsweringService(), messageSystem.getAddressService().getMessageProcessor(), error, connection);
            messageSystem.sendMessage(sendingMsg);
            //connection.sendMessage(errorsCodes.get("INVALID_REQUEST"));
        }
    }
}
