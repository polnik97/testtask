package ru.rti.task.implementation.cofigurationImplement;

import ru.rti.task.Server;
import ru.rti.task.configuration.Configuration;
import ru.rti.task.configuration.InitialConfiguration;

import java.util.HashMap;
import java.util.Map;

public class ServerConfiguration implements Configuration{

    private int port;
    private String host;
    private Map<String, String> erorCodes;
    private int handledMassageCount;

    public ServerConfiguration()
    {
        InitialConfigurationImpl inConf = new InitialConfigurationImpl();
        this.port = (int)inConf.getPort();
        this.host = inConf.getHost();
        this.erorCodes = inConf.getErorCodes();

    }

    public ServerConfiguration(String startHost, int startPort, Map<String, String> er, int mesCount)
    {
        this.port = startPort;
        this.host = startHost;
        this.erorCodes = er;
        this.handledMassageCount = mesCount;
    }

    public ServerConfiguration(AbstractInitialConfiguration inConf)
    {
        this.port = (int)inConf.getPort();
        this.host = inConf.getHost();
        this.erorCodes = inConf.getErorCodes();
    }


    @Override
    public String getHostName()
    {
        return this.host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    @Override
    public int getPort()
    {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    @Override
    public Map<String, String> getErrorCodesMapping()
    {
        return this.erorCodes;
    }

    public void setErorCodes(Map<String, String> erorCodes) {
        this.erorCodes = erorCodes;
    }

    @Override
    public int getHandledMessagesCount()
    {
        return this.handledMassageCount;
    }


}
