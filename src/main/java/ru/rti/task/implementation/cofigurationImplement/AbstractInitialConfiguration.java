package ru.rti.task.implementation.cofigurationImplement;

import com.sun.tools.doclets.formats.html.SourceToHTMLConverter;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import ru.rti.task.configuration.Configuration;
import ru.rti.task.configuration.InitialConfiguration;
import ru.rti.task.constants.ConfigurationFormat;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.w3c.dom.Node;

import static java.lang.Thread.sleep;

public abstract class AbstractInitialConfiguration implements InitialConfiguration {

    private long port;
    private String host;

    public long getPort() {
        return port;
    }

    public void setPort(long port) {
        this.port = port;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public Map<String, String> getErorCodes() {
        return erorCodes;
    }

    public void setErorCodes(Map<String, String> erorCodes) {
        this.erorCodes = erorCodes;
    }

    public void setFormat(ConfigurationFormat format) {
        this.format = format;
    }

    private Map<String, String> erorCodes;
    private ConfigurationFormat format = ConfigurationFormat.JSON;


    AbstractInitialConfiguration() {
        erorCodes = new HashMap<String, String>();
        this.setValues();
    }

    @Override
    public ConfigurationFormat getFormat() {
        return format;
    }


    //выгрузга в строку информации из файлов конфигурации
    @Override
    public String getContent()  {

        String content = "";

        if(format == ConfigurationFormat.JSON) {
            FileReader fr = null;
            try {
                fr = new FileReader("/Users/Mac/IdeaProjects/rti-candidate-task_2/src/main/java/ru/rti/task/implementation/configurationFiles/configure.json");
                Scanner scan = new Scanner(fr);
                while (scan.hasNextLine()) {
                    content = new StringBuilder(content).append(scan.nextLine()).toString();
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            try {
                fr.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else
        {
            FileReader fr = null;
            try {
                fr = new FileReader("/Users/Mac/IdeaProjects/rti-candidate-task_2/src/main/java/ru/rti/task/implementation/configurationFiles/configure.xml");
                Scanner scan = new Scanner(fr);
                while (scan.hasNextLine()) {
                    content = content + scan.nextLine();
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            try {
                fr.close();
            } catch (IOException | NullPointerException e) {
                e.printStackTrace();
            }
        }
        return content;
    }

    //парсинг и установка значений класса из файла конфигурации
    public void setValues()
    {
        //парс JSON
        if(format == ConfigurationFormat.JSON)
        {
            JSONParser parser = new JSONParser();
            try {
                Object obj = parser.parse(this.getContent());
                JSONObject jsonObf = (JSONObject) obj;
                port = (long)jsonObf.get("port");

                host = (String) jsonObf.get("host");

                JSONObject ers = (JSONObject) jsonObf.get("errorCodesMapping");
                for(Object erCode : ers.keySet())
                {
                    erorCodes.put( (String) erCode, (String) ers.get(erCode));
                }
                try {
                    sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            } catch (ParseException e) {
                e.printStackTrace();
            }
        }else //парс XML
        {
            try {

                // Строим объектную модель исходного XML файла
                DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                DocumentBuilder db = dbf.newDocumentBuilder();
                InputSource is = new InputSource(new StringReader(getContent()));
                Document doc = db.parse(new File("/Users/Mac/IdeaProjects/rti-candidate-task_2/src/main/java/ru/rti/task/implementation/configurationFiles/configure.xml"));



                // Выполняем нормализацию
                doc.getDocumentElement().normalize();
                // Получение значения "host"
                NodeList confs = doc.getElementsByTagName("host");
                Element el = (Element) confs.item(0);
                this.host = el.getTextContent();
                // Получение значения "port"
                confs = doc.getElementsByTagName("port");
                el = (Element) confs.item(0);
                this.port = Integer.parseInt(el.getTextContent().trim());

                // Получение значения "errorCodes"
                confs = doc.getElementsByTagName("errorCodes");
                NodeList erCodes = confs.item(0).getChildNodes();
                for(int i = 0; i < erCodes.getLength(); i++)
                {
                    Node childEl =  erCodes.item(i);
                    if( childEl.getNodeName().trim().equals("#text")) {
                        continue;
                    }
                    erorCodes.put(childEl.getNodeName() ,childEl.getTextContent());
                }

            } catch (ParserConfigurationException | IOException ex) {

            } catch (SAXException e) {
                e.printStackTrace();
            }
        }
    }
}
