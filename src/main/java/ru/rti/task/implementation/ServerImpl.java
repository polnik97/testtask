package ru.rti.task.implementation;

import ru.rti.task.Server;
import ru.rti.task.configuration.InitialConfiguration;
import ru.rti.task.constants.ServerStatus;
import ru.rti.task.implementation.answeringService.AnsveringService;
import ru.rti.task.implementation.cofigurationImplement.InitialConfigurationImpl;
import ru.rti.task.implementation.cofigurationImplement.ServerConfiguration;
import ru.rti.task.implementation.holdingConnections.ConnectionsChecker;
import ru.rti.task.implementation.holdingConnections.ConnectionsHolder;
import ru.rti.task.implementation.holdingConnections.TCPConnection;
import ru.rti.task.implementation.holdingConnections.TCPConnectionListener;
import ru.rti.task.implementation.messageProcessing.MessageProcessor;
import ru.rti.task.implementation.messageSystem.MessageSystem;
import ru.rti.task.state.ServerState;

import java.io.IOException;
import java.net.ServerSocket;

import static java.lang.Thread.sleep;

public class ServerImpl implements Server, TCPConnectionListener {

    private static ServerSocket server;
    //Хранится конфигурация сервера
    private ServerConfiguration configure;
    //Статус сервера
    private ServerStatus serverStatus = ServerStatus.DOWN;
    private MessageSystem messageSystem;
    private ConnectionsHolder connectionsHolder;

    private Thread connectionsChecker;
    private Thread messageProcessor;
    private Thread answeringService;



    public static void main(String[] args) {
        new ServerImpl();
    }


    public ServerImpl() {
        configure = new ServerConfiguration();
        messageSystem = new MessageSystem();
        connectionsHolder = new ConnectionsHolder();
        //запуск сервера
        run();
        //обработка входящих подключений
        handle();
        //завершение работы сервера
        stop();
    }

    @Override
    public void configure(InitialConfiguration initialConfiguration) {

        InitialConfigurationImpl startConf = new InitialConfigurationImpl();

        this.configure.setHost(startConf.getHost());
        this.configure.setPort((int)startConf.getPort());
        this.configure.setErorCodes(startConf.getErorCodes());
    }
    //запуск сервера
    @Override
    public void run() {
        if(this.serverStatus == ServerStatus.DOWN) {
            try {
                this.server = new ServerSocket(configure.getPort());
                connectionsChecker =  new Thread(new ConnectionsChecker(connectionsHolder, messageSystem));
                messageProcessor = new Thread(new MessageProcessor(messageSystem));
                answeringService = new Thread(new AnsveringService(messageSystem));

                connectionsChecker.start();
                messageProcessor.start();
                answeringService.start();
                System.out.println("Server is running...");
                this.serverStatus = ServerStatus.UP;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else
        {
            System.out.println("Couldn't start runnung server.");
        }
    }

    //обработка входящих подключений
    public void handle()
    {
        while(true)
        {
            try {
                connectionsHolder.addConnection(new TCPConnection(this, server.accept())); //создание нового подключения
            } catch (IOException e) {
                System.out.println("TCPConnection exeption: " + e);
            }
        }

    }
    //остановка сервера
    @Override
    public void stop() {
        try {
            server.close();
            answeringService.interrupt();
            connectionsChecker.interrupt();
            messageProcessor.interrupt();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public ServerState getState() {
        return null;
    }

    //дейстиве при создание нового подключения
    @Override
    public synchronized void onConnectionReady(TCPConnection connection) {
        System.out.println("New connection: " + connection.getSocket().getInetAddress() + " " + connection.getSocket().getPort());
    }

    //Обработка входящего сообщения
    @Override
    public synchronized void onReceieviMessage(TCPConnection connection, String msg) {

    }

    //реакция на ошибку
    @Override
    public synchronized void onExeption(TCPConnection connection, Exception e) {
        System.out.println("TCPConnection exeption: " + e);
    }

    //реакция на отсоединение
    @Override
    public synchronized void onDisconnect(TCPConnection connection) {

    }
}
