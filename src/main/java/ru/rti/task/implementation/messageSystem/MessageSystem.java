package ru.rti.task.implementation.messageSystem;

import ru.rti.task.implementation.cofigurationImplement.InitialConfigurationImpl;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CopyOnWriteArrayList;

//Хранение сообщений для разных сервисов в очереди

public final class MessageSystem {
    private final Map<Address, ConcurrentLinkedQueue<Message>> messages = new HashMap<>();
    private final AddressService addressService = new AddressService();
    private final Map<String, String> errors;

    public MessageSystem() {
        errors = new InitialConfigurationImpl().getErorCodes();
    }

    public Map<String, String> getErrors() {
        return errors;
    }

    public Map<Address, ConcurrentLinkedQueue<Message>> getMessages() {
        return messages;
    }

    public AddressService getAddressService() {
        return addressService;
    }

    public void addService(Abonent abonent) {
        messages.put(abonent.getAddress(), new ConcurrentLinkedQueue<>());
    }

    public void sendMessage(Message message) {
        messages.get(message.getTo()).add(message);
    }
}
