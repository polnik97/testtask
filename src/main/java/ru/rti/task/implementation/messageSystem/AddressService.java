package ru.rti.task.implementation.messageSystem;

import ru.rti.task.implementation.answeringService.AnsveringService;
import ru.rti.task.implementation.holdingConnections.ConnectionsChecker;
import ru.rti.task.implementation.holdingConnections.ConnectionsHolder;
import ru.rti.task.implementation.messageProcessing.MessageProcessor;

//Управления адрессами сервисов и хранение их адресов

public class AddressService {
    private Address answeringService;
    private Address messageProcessor;

    public void registerAnsweringService(AnsveringService ansveringService) {
        this.answeringService = ansveringService.getAddress();
    }

    public void registerMessageProcessor(MessageProcessor messageProcessor) {
        this.messageProcessor = messageProcessor.getAddress();
    }

    public void registerConnectionsChecker(ConnectionsChecker connectionsChecker) {
        this.messageProcessor = connectionsChecker.getAddress();
    }

    public Address getAnsweringService() {
        return this.answeringService;
    }

    public Address getMessageProcessor() {
        return this.messageProcessor;
    }
}
