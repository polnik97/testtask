package ru.rti.task.implementation.messageSystem;

import java.util.concurrent.atomic.AtomicInteger;

//Уникальный адрес для каждого модуля

public final class Address {

    private static final AtomicInteger ID_GENERATOR = new AtomicInteger();
    private final int id;

    public Address(){
        id = ID_GENERATOR.getAndIncrement();
    }

    @Override
    public int hashCode() {
        return id;
    }
}
