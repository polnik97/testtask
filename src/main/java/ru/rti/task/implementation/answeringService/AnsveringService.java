package ru.rti.task.implementation.answeringService;

import ru.rti.task.implementation.messageSystem.Abonent;
import ru.rti.task.implementation.messageSystem.Address;
import ru.rti.task.implementation.messageSystem.Message;
import ru.rti.task.implementation.messageSystem.MessageSystem;

import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static java.lang.Thread.sleep;

// Постоянно проверяет очередь на наличие сообщений для позьзователя
// если получает, то закидывает в пулл и выполняет

public class AnsveringService implements Abonent, Runnable {

    private final Address address = new Address();
    private final MessageSystem messageSystem;

    public AnsveringService(MessageSystem messageSystem)
    {
        this.messageSystem = messageSystem;
        messageSystem.addService(this);
        messageSystem.getAddressService().registerAnsweringService(this);
    }


    @Override
    public void run() {
        ExecutorService pool = Executors.newFixedThreadPool(3);
        while(true) {
            ConcurrentLinkedQueue<Message> messages = messageSystem.getMessages().get(address);
            while (!messages.isEmpty())
            {
                try {
                    pool.submit(messages.poll());
                }catch (NullPointerException e)
                {
                    continue;
                }

            }
        }

    }

    public MessageSystem getMessageSystem(){
        return messageSystem;
    }

    @Override
    public Address getAddress() {
        return this.address;
    }
}
