package ru.rti.task.implementation.answeringService;

import ru.rti.task.implementation.holdingConnections.TCPConnection;
import ru.rti.task.implementation.messageSystem.Abonent;
import ru.rti.task.implementation.messageSystem.Address;
import ru.rti.task.implementation.messageSystem.Message;

//простой ответ в виде строки, исспользует экземпляр класса TCPConnection

public class SimpleAnswer extends Message implements Runnable {

    private final String answer;
    private final TCPConnection connection;

    public SimpleAnswer(Address from, Address to, String answer, TCPConnection connection ) {
        super(from, to);
        this.answer = answer;
        this.connection = connection;
    }

    @Override
    public void run() {

        if(!connection.getSocket().isClosed())
        {
            connection.sendMessage(answer);
        }

    }
}
